import axios from "axios";

import { logTodo } from "./helpers";
import { TodoInterface } from "./interfaces";

const url: string = "https://jsonplaceholder.typicode.com/todos/1";

axios.get(url).then((response) => {
  const todo = response.data as TodoInterface;

  const id = todo.id;
  const title = todo.title;
  const completed = todo.completed;

  logTodo(id, title, completed);
});
