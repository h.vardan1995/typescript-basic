"use strict";
exports.__esModule = true;
exports.logTodo = void 0;
var logTodo = function (id, title, completed) {
    console.log("\n      The Todo with ID: ".concat(id, "\n      Has a title of: ").concat(title, "\n      Is it completed? ").concat(completed, " \n     "));
};
exports.logTodo = logTodo;
